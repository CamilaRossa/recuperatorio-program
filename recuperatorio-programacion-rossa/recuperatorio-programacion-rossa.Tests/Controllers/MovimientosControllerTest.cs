﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using recuperatorio_programacion_rossa;
using recuperatorio_programacion_rossa.Controllers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Web.Http;

namespace recuperatorio_programacion_rossa.Tests.Controllers
{
    [TestClass]
    public class MovimientosControllerTest
    {
        //LOS TESTS DEBIAN SER SOBRE LOGICA DE NEGOCIO Y NO SOBRE LOS CONTROLLERS

        [TestMethod]
        public void Post_DatosCorrectos_DevuelveMensajeOk()
        {
             // Disponer
            int dniEmisor = dniEmi;
            int dniReceptor = dniRec;
            string rdoEsperado = "movimiento correcto";
            MovimientosController controller = new MovimientosController();

            // Actuar
            var result = controller.Validate(dniEmisor, dniReceptor);
            controller.Post("movimiento");

            // Declarar
            Assert.IsNotNull(rdoEsperado, result);
        }
        [TestMethod]
        public void Post_DatosIncorrectos_DevuelveMensajeError()
        {
            // Disponer
            int dniEmisor = dniEmi;
            int dniReceptor = dniRec;
            string rdoEsperado = "movimiento correcto";
            MovimientosController controller = new MovimientosController();

            // Actuar
            var result = controller.Validate(dniEmisor, dniReceptor);
            controller.Post("movimiento");

            // Declarar
            Assert.IsNotNull(rdoEsperado, result);
        }

    }
}
