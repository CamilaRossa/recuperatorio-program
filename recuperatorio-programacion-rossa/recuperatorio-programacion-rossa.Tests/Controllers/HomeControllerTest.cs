﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using recuperatorio_programacion_rossa;
using recuperatorio_programacion_rossa.Controllers;
using System.Web.Mvc;

namespace recuperatorio_programacion_rossa.Tests.Controllers
{
    [TestClass]
    public class HomeControllerTest
    {
        [TestMethod]
        public void Index()
        {
            // Disponer
            HomeController controller = new HomeController();

            // Actuar
            ViewResult result = controller.Index() as ViewResult;

            // Declarar
            Assert.IsNotNull(result);
            Assert.AreEqual("Home Page", result.ViewBag.Title);
        }
    }
}
