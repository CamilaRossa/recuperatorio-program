﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Logica;

namespace recuperatorio_programacion_rossa.Models
{
    public class MovimientoRequest
    {
        public int DniEmisor { get; set; }
        public int DniReceptor { get; set; }
        public string Descripcion { get; set; }
        public decimal Monto { get; set; }
        public Movimiento CrearMovimiento()
        {
            Movimiento movimiento = new Movimiento(this.DniEmisor, this.DniReceptor, this.Descripcion, this.Monto);
            return movimiento;
        }
    }
}