﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Logica;
using recuperatorio_programacion_rossa.Models;
namespace recuperatorio_programacion_rossa.Controllers
{
    public class MovimientosController : ApiController
    {
        Principal principal = Principal.Instance;
        public IHttpActionResult Post([FromBody] MovimientoRequest nuevoMovimiento)
        {
            Movimiento movimiento = nuevoMovimiento.CrearMovimiento();
            Resultado resultado = principal.EnviarDinero(movimiento);

            if (resultado.estado)
            {
                return Content(HttpStatusCode.Created, resultado.estado);
            }
            return Content(HttpStatusCode.BadRequest, resultado);

        }

        [Route("/{dni}")]
        public IHttpActionResult Get(int dni)
        {
            var usuario = principal.ValidarExistenciaUsuario(dni);
            if (usuario == null)
            {
                return Content(HttpStatusCode.BadRequest, "No existe el usuario");
            }
            else
            {
                var lista = usuario.ListaMovimientos.OrderByDescending(x => x.Fecha).ToList();
                return Content(HttpStatusCode.OK, lista);
            }
        }
        [Route("{id}")]//asignar repartidor a un envio menor distancia entre desti y rep
        public IHttpActionResult Delete(int id)
        {
            Movimiento movAEliminar = principal.ListaMovimientos.Find(x => x.id == id);
            if (movAEliminar == null)
                return Content(HttpStatusCode.BadRequest, "No existe el movimiento");

            Resultado resultado = principal.CancelarMovimiento(movAEliminar);
            if (resultado.estado)
            {
                return Content(HttpStatusCode.OK, resultado.ID);
            }
            return Content(HttpStatusCode.BadRequest, resultado);
        }
    }
}
