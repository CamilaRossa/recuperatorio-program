﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Usuario
    {
        public int Dni { get; set; }
        public string NombreApellido { get; set; }
        public decimal Saldo { get; set; }
        //BUENA PROPUESTA
        public List<Movimiento> ListaMovimientos { get { return Principal.Instance.ListaMovimientos.FindAll(x => x.DniEmisor == Dni || x.DniReceptor == Dni); } }

        //ESTE METODO ES BUENO. AUNQUE PODRIAS PENSAR EN UN METODO "CREARTRANSACCION" QUE HAGA LAS 2 COSAS
        //AGREGAR EL MOV AL LISTADO Y ACTUALIZAR SALDO
        //DE ESA MANERA NO NECESITAS EN PRINCIPAL ACORDATE DE HACERLO Y EN EL ORDEN CORRECTO.
        public void ActualizarSaldo()
        {
            this.Saldo = ListaMovimientos.Sum(x => x.Monto);
        }
    }
}
