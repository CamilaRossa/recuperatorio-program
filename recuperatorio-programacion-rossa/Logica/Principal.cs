﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public sealed class Principal
    {
        public List<Usuario> ListaUsuarios { get; set; }
        public List<Movimiento> ListaMovimientos { get; set; }

        private readonly static Principal _instance = new Principal();

        private Principal()
        {
            if (ListaUsuarios == null)
                ListaUsuarios = new List<Usuario>();
            if (ListaMovimientos == null)
              ListaMovimientos = new List<Movimiento>();

        }

        public static Principal Instance
        {
            get
            {
                return _instance;
            }
        }

        public Resultado EnviarDinero(Movimiento movimiento)
        {
            Usuario Emisor = this.ListaUsuarios.Find(x => x.Dni == movimiento.DniEmisor);
            if (Emisor != null)
                return new Resultado(false, "No existe el usuario Emisor");
            Usuario Receptor = this.ListaUsuarios.Find(x => x.Dni == movimiento.DniReceptor);
            if (Receptor != null)
                return new Resultado(false, "No existe el usuario Receptor");
            if (Emisor.Saldo < movimiento.Monto)
                return new Resultado(false, "Saldo insuficiente");
            Movimiento movEnv = movimiento;
            movEnv.Monto = movEnv.Monto * -1;
            movEnv.id = Movimiento.GenerarID();

            ListaMovimientos.Add(movEnv);
            ListaMovimientos.Add(movimiento);

            //REVISAR MENSAJES EN EL METODO
            Emisor.ActualizarSaldo();
            Receptor.ActualizarSaldo();

            return new Resultado(true);
        }

        public Usuario ValidarExistenciaUsuario(int dni)
        {
           return this.ListaUsuarios.Find(x=>x.Dni == dni);
        }

        public Resultado CancelarMovimiento(Movimiento movimiento)
        {
            Usuario Emisor = this.ListaUsuarios.Find(x => x.Dni == movimiento.DniEmisor);
            Usuario Receptor = this.ListaUsuarios.Find(x => x.Dni == movimiento.DniReceptor);
            Movimiento movCancelacion = movimiento;
            movCancelacion.Monto = movCancelacion.Monto * -1;
            movCancelacion.id = Movimiento.GenerarID();
            movCancelacion.Descripcion = "Cancelacion" + movCancelacion.Descripcion;
            ListaMovimientos.Add(movCancelacion);
            Emisor.ActualizarSaldo();
            Receptor.ActualizarSaldo();

            return new Resultado(true);
        }
    }
}
