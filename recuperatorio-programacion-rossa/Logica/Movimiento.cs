﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Movimiento
    {
        public int DniEmisor { get; set; }
        public int DniReceptor { get; set; }
        public string Descripcion { get; set; }
        public DateTime Fecha { get; set; }
        public int id { get; set; }
        public decimal Monto { get; set; }
        public static int GenerarID()
        {
            Random ran = new Random();
            return ran.Next(0, 10000000);

        }

        public Movimiento(int emi, int rec, string dec, decimal monto)
        {
            this.Fecha = DateTime.Today;
            this.id = GenerarID();
            this.DniEmisor = emi;
            this.DniReceptor = rec;
            this.Descripcion = dec;
            this.Monto = monto;
        }
    }
}
