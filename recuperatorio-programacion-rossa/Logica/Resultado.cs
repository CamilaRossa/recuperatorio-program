﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Logica
{
    public class Resultado
    {
        public string ID { get; set; }
        public bool estado { get; set; }
        public string error { get; set; }

        public Resultado(string id, bool Ok)
        {
            this.ID = id;
            this.estado = true;
        }

        public Resultado(bool estado, string error)
        {
            this.estado = false;
            this.error = error;
        }

        public Resultado(bool v)
        {
            this.estado = v;
        }
    }
}
